<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_kategori extends CI_Model {

	public function tampil_kat()
	{
		$tm_kategori= $this->db->get('data_kategori_buku')->result();
		return $tm_kategori;
	}
	public function simpan_kat()
	{
		$object = array(
			'nama_kategori' =>$this->input->post('nama_kategori') );
		return $this->db->insert('data_kategori_buku', $object);
	}
	public function detail($a)
	{
		return $this->db->where('kode_kategori', $a)
					->get('data_kategori_buku')
					->row();
	}
	public function edit_kat()
	{
		$object = array(
			'nama_kategori' =>$this->input->post('nama_kategori')
		);
		return $this->db->where('kode_kategori',$this->input->post('kode_kategori_lama'))->update('data_kategori_buku',$object);
	}
	public function hapus_kat($id='')
	{
		return $this->db->where('kode_kategori', $id)->delete('data_kategori_buku');
	}

}

/* End of file M_kategori.php */
/* Location: ./application/models/M_kategori.php */