<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_buku extends CI_Model {

	public function tampil_buku()
		{
			$tm_buku=$this->db
						  ->join('data_kategori_buku', 'data_kategori_buku.kode_kategori = data_buku.kode_kategori')
						  ->get('data_buku')->result();
						  return $tm_buku;
		}	
		public function data_kategori()
		{
			return $this->db->get('data_kategori_buku')->result();
		}
		public function simpan_buku($nama_file)
		{
			if ($nama_file=="") {
				$object=array(
					'judul_buku' => $this->input->post('judul_buku'),
					'kode_kategori' => $this->input->post('kode_kategori'),
					'tahun' => $this->input->post('tahun'),
					'penulis' => $this->input->post('penulis'),
					'penerbit' => $this->input->post('penerbit'),
					'harga' => $this->input->post('harga'),
					'diskon' => $this->input->post('diskon'),
					'stok' => $this->input->post('stok'),
				);
			}else{
				$object=array(
					'judul_buku' => $this->input->post('judul_buku'),
					'kode_kategori' => $this->input->post('kode_kategori'),
					'tahun' => $this->input->post('tahun'),
					'penulis' => $this->input->post('penulis'),
					'penerbit' => $this->input->post('penerbit'),
					'harga' => $this->input->post('harga'),
					'diskon' => $this->input->post('diskon'),
					'stok' => $this->input->post('stok'),
					'foto_cover' => $nama_file
				);
			}
			return $this->db->insert('data_buku', $object);
		}
		public function detail($a)
		{		

			$tm_buku=$this->db
						  ->join('data_kategori_buku', 'data_kategori_buku.kode_kategori = data_buku.kode_kategori')
						  ->where('kode_buku',$a)
						  ->get('data_buku')
						  ->row();
		    $total = $tm_buku->stok;
			$total --;
			$object = array('stok'=>$total);
			$this->db->where('kode_buku', $a)->update('data_buku',$object);
			return $tm_buku;
		}
		public function buku_update_no_foto()
		{
			$object = array(
				'judul_buku' => $this->input->post('judul_buku'),
					'kode_kategori' => $this->input->post('kode_kategori'),
					'tahun' => $this->input->post('tahun'),
					'penulis' => $this->input->post('penulis'),
					'penerbit' => $this->input->post('penerbit'),
					'harga' => $this->input->post('harga'),
					'diskon' => $this->input->post('diskon'),
					'stok' => $this->input->post('stok')
				);
			return $this->db->where('kode_buku', $this->input->post('kode_buku'))
						->update('data_buku',$object);
		}
		public function buku_update_dengan_foto($nama_foto='')
		{
			$object=array(
					'judul_buku' => $this->input->post('judul_buku'),
					'kode_kategori' => $this->input->post('kode_kategori'),
					'tahun' => $this->input->post('tahun'),
					'penulis' => $this->input->post('penulis'),
					'penerbit' => $this->input->post('penerbit'),
					'harga' => $this->input->post('harga'),
					'diskon' => $this->input->post('diskon'),
					'stok' => $this->input->post('stok'),
					'foto_cover' => $nama_foto
				);
			return $this->db->where('kode_buku', $this->input->post('kode_buku'))
						->update('data_buku',$object);
		}
		public function hapus_buku($id_buku='')
		{
			return $this->db->where('kode_buku', $id_buku)->delete('data_buku');
		}

}

/* End of file M_buku.php */
/* Location: ./application/models/M_buku.php */