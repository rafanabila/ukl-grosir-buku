<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_transaksi extends CI_Model {

	public function tm_user()
	{
		return $this->db->get('data_user')->result();
	}
	public function simpan_cart_db()
	{
		$object = array(
			'kode_user' => $this->session->userdata('kode_user'),
			'tanggal_beli' => date('Y-m-d'),
			'total' => $this->input->post('grandtotal'),
			'uang' => $this->input->post('uang')
		);
		$this->db->insert('data_transaksi', $object);
		$tm_nota=$this->db->order_by('kode_transaksi', 'desc')
					->where('kode_user',$this->session->userdata('kode_user'))
					->limit(1)
					->get('data_transaksi')
					->row();
		for ($i=0; $i <count($this->input->post('rowid')) ; $i++) { 
				$hasil[] = array(
					'kode_transaksi' => $tm_nota->kode_transaksi , 
					'kode_buku' => $this->input->post('kode_buku')[$i],
					'jumlah' => $this->input->post('qty')[$i]
				);
			}	
			$proses=$this->db->insert_batch('detil_transaksi',$hasil);
			if ($proses) {
				return $tm_nota->kode_transaksi;
			}else{
				return 0;
			}
	}
	public function detail_nota($id_nota)
	{
		return $this->db->where('kode_transaksi', $id_nota)
					->join('data_user','data_user.kode_user=data_transaksi.kode_user')
					->get('data_transaksi')->row();
	}
	public function detail_transaksi($id_nota)
	{
		return $this->db->where('kode_transaksi', $id_nota)
					->join('data_buku','data_buku.kode_buku=detil_transaksi.kode_buku')
					->join('data_kategori_buku','data_kategori_buku.kode_kategori= data_buku.kode_kategori')
					->get('detil_transaksi')->result();
	}

}

/* End of file M_transaksi.php */
/* Location: ./application/models/M_transaksi.php */