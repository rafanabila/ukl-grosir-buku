<h2 align="center">Data User Grosir Buku KinanMedia</h2>
<?=$this->session->flashdata('pesan');?>
<center><a href="#tambah" data-toggle="modal" class="btn btn-warning">Tambah</a></center>
<table class="table table-hover table-striped">
	<thead>
		<tr>
			<td>No</td>
			<td>Id User</td>
			<td>Username</td>
			<td>Password</td>
			<td>Level</td>
			<td>Aksi</td>
		</tr>
	</thead>
	<tbody>
		<?php $no=0; foreach($tampil_user as $kas): 
		$no++;?>
		<tr>
			<td><?=$no?></td>
			<td><?=$kas->kode_user?></td>
			<td><?=$kas->username?></td>
			<td><?=$kas->password?></td>
			<td><?=$kas->level?></td>
			<td>
				<a href="#edit" onclick="edit('<?=$kas->kode_user?>')" data-toggle="modal" clas="btn btn-success">Ubah</a>
				<a href="<?=base_url('index.php/user/hapus/'.$kas->kode_user)?>" onclick="return confirm('apakah anda yakin?')" clas="btn btn-danger">Hapus</a>
			</td>
		</tr>
		<?php endforeach ?>
	</tbody>
</table>

<div class="modal fade" id="tambah">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4>Tambah User</h4>
			</div>
			<div class="modal-body">
				<form action="<?=base_url('index.php/user/tambah')?>" method="post">
					<table>
						<!--<tr>
							<td>Id Kategori</td>
							<td><input type="text" name="kode_kategori" required class="form-control"></td>
						</tr>-->
						<tr>
							<td>Nama User</td>
							<td><input type="text" name="username" required class="form-control"></td>			
						</tr>
						<tr>
							<td>Password</td>
							<td><input type="text" name="password" required class="form-control"></td>
						</tr>
						<tr>
						<td>Level</td>
							<td><input type="text" name="level" required class="form-control"></td>	
						</tr>
					</table>
					<input type="submit" name="simpan" value="Simpan" class="btn btn-success">
				</form>
			</div>
		</div>
		
	</div>
</div>


<div class="modal fade" id="edit">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4>Ubah User</h4>
			</div>
			<div class="modal-body">
				<form action="<?=base_url('index.php/user/user_update')?>" method="post">
					<input type="hidden" name="kode_user_lama" id="kode_user_lama">
					<table>
						<tr>
							<td>Nama user</td>
							<td><input type="text" name="username" required class="form-control" id="username"></td>
						</tr>
						<tr>
							<td>Password</td>
							<td><input type="text" name="password" required class="form-control" id="password"></td>
						</tr>
						<tr>
						<td>Level</td>
							<td><input type="text" name="level" required class="form-control" id="level"></td>	
						</tr>
					</table>
					<input type="submit" name="edit" value="Simpan" class="btn btn-success">
				</form>
			</div>
		</div>
		
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('#example').DataTable();
	});
</script>
<script type="text/javascript">
	function edit(a) {
		$.ajax({
			type:"post",
			url:"<?=base_url()?>index.php/user/edit_user/"+a,
			dataType:"json",
			success:function(data){
				$("#kode_user").val(data.kode_user);
				$("#username").val(data.username);
				$("#password").val(data.password);
				$("#level").val(data.level);
				$("#kode_user_lama").val(data.kode_user);
			}
		});
	}
</script>