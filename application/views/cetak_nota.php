<link rel="stylesheet" href="<?=base_url()?>assets/vendor/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="<?=base_url()?>assets/vendor/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="<?=base_url()?>assets/vendor/linearicons/style.css">
<link rel="stylesheet" href="<?=base_url()?>assets/vendor/chartist/css/chartist-custom.css">
	<!-- MAIN CSS -->
<link rel="stylesheet" href="<?=base_url()?>assets/css/main.css">
<h2 align="center">Nota</h2>
<h4 align="center">Toko Grosir Buku KinanMedia</h4>
No Nota : <?= $nota->kode_transaksi?><br>
Tanggal Beli : <?= $nota->tanggal_beli ?><br>
Kasir : <?= $this->session->userdata('nama_user')?>
<table class="table table-hover table-striped" border="1" style="border-collapse: collapse;">
	<thead>
	<tr>
		<th>NO</th>
		<th>Nama Buku</th>
		<th>Harga</th>
		<th>QTY</th>
		<th>Subtotal</th>
		
	</tr>
</thead>
	<?php $no=0; foreach($this->trans->detail_transaksi($nota->kode_transaksi) as $buku) : $no++;
	$diskonan=$buku->harga-($buku->diskon*$buku->harga/100);
	?>
<tbody>
	<tr>
		<th><?=$no?></th>
		<th><?=$buku->judul_buku?></th>
		<th><?= number_format($diskonan)?></th>
		<th><?=$buku->jumlah?></th>		
		<th><?=number_format(($diskonan*$buku->jumlah))?></th>
	</tr>
<?php endforeach?>
<tr>
	<th>Grandtotal</th>
	<th colspan="4" align="center"><?= number_format($nota->total)?></th>
</tr>
<tr>
	<th>Bayar</th>
	<th colspan="4" align="center"><?=$nota->uang?></th>
</tr>
<tr>
	<th>Kembalian</th>
	<th colspan="4" align="center"><?= number_format($nota->uang-$nota->total)?></th>
</tr>
</tbody>
</table>

<script type="text/javascript">
	window.print();
	location.href="<?=base_url('index.php/transaksi')?>";
</script>