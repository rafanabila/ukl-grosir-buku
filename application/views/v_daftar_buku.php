<h2 align="center">Daftar Buku Grosir KinanMedia</h2>
<?=$this->session->flashdata('pesan');?>
<center><a href="#tambah" data-toggle="modal" class="btn btn-warning">Tambah</a></center>
<table class="table table-hover table-striped">
	<thead>
		<tr>
			<td>No</td>
			<td>Gambar</td>
			<td>Judul</td>
			<td>Kategori</td>
			<td>Tahun</td>
			<td>Penulis</td>
			<td>Penerbit</td>		
			<td>Harga</td>
			<td>Diskon</td>
			<td>Stok</td>
			<td>Aksi</td>
		</tr>
	</thead>
	<tbody>
		<?php $no=0; foreach($tampil_buku as $buku): 
		$no++;?>
		<tr>
			<td><?=$no?></td>
			<td><img src="<?=base_url('assets/gambar/'.$buku->foto_cover)?>" style="width: 40px"></td>
			<td><?=$buku->judul_buku?></td>
			<td><?=$buku->nama_kategori?></td>
			<td><?=$buku->tahun?></td>
			<td><?=$buku->penulis?></td>
			<td><?=$buku->penerbit?></td>
			<td><?=$buku->harga?></td>
			<td><?=$buku->diskon?></td>
			<td><?=$buku->stok?></td>
			<td>
				<a href="#edit" onclick="edit('<?=$buku->kode_buku?>')" data-toggle="modal" clas="btn btn-success">Ubah</a>
				<a href="<?=base_url('index.php/buku/hapus/'.$buku->kode_buku)?>" onclick="return confirm('apakah anda yakin?')" clas="btn btn-danger">Hapus</a>
			</td>
		</tr>
		<?php endforeach ?>
	</tbody>
</table>


<div class="modal fade" id="tambah">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4>Tambah Daftar Buku</h4>
			</div>
			<div class="modal-body">
				<form action="<?=base_url('index.php/buku/tambah/')?>" method="post" enctype="multipart/form-data">
					<table>
						<tr>
							<td>Judul buku</td>
							<td><input type="text" name="judul_buku" class="form-control"></td>
						</tr>					
						<tr>
							<td>Gambar</td>
							<td><input type="file" name="foto_cover" class="form-control"></td>
						</tr>
						<tr>
							<td>Kategori</td>
							<td><select name="kode_kategori" class="form-control">
								<option></option>
								<?php foreach ($kategori as $kat): ?>
								<option value="<?=$kat->kode_kategori?>">
									<?=$kat->nama_kategori?>
								<?php endforeach ?>
								</option>
							</select></td>
						</tr>
						<tr>
							<td>Tahun</td>
							<td><input type="number" name="tahun" class="form-control"></td>
						</tr>						
						<tr>
							<td>Penulis</td>
							<td><input type="text" name="penulis" class="form-control"></td>
						</tr>
						<tr>
							<td>Penerbit</td>
							<td><input type="text" name="penerbit" class="form-control"></td>
						</tr>
						<tr>
							<td>Harga</td>
							<td><input type="number" name="harga" class="form-control"></td>
						</tr>
						<tr>
							<td>Diskon</td>
							<td><input type="number" name="diskon" class="form-control"></td>
						</tr>
						<tr>
							<td>Stok</td>
							<td><input type="number" name="stok" class="form-control"></td>
						</tr>
					</table>
					<input type="submit" name="simpan" value="Simpan" class="btn btn-success">
				</form>
			</div>
		</div>
		
	</div>
</div>


<div class="modal fade" id="edit">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4>Ubah Daftar Buku</h4>
			</div>
			<div class="modal-body">
				<form action="<?=base_url('index.php/buku/buku_update')?>" method="post" enctype="multipart/form-data">
					<input type="hidden" name="kode_buku" id="kode_buku">
					<table>
						<tr>
							<td>Judul buku</td>
							<td><input type="text" id="judul_buku" name="judul_buku" class="form-control"></td>
						</tr>					
						<tr>
							<td>Gambar</td>
							<td><input type="file" id="foto_cover" name="foto_cover" class="form-control"></td>
						</tr>
						<tr>
							<td>Kategori</td>
							<td><select name="kode_kategori" id="kode_kategori" class="form-control">
								<option></option>
								<?php foreach ($kategori as $kat): ?>
								<option value="<?=$kat->kode_kategori?>">
									<?=$kat->nama_kategori?>
								<?php endforeach ?>
								</option>
							</select></td>
						</tr>
						<tr>
							<td>Tahun</td>
							<td><input type="number" id="tahun" name="tahun" class="form-control"></td>
						</tr>						
						<tr>
							<td>Penulis</td>
							<td><input type="text" id="penulis" name="penulis" class="form-control"></td>
						</tr>
						<tr>
							<td>Penerbit</td>
							<td><input type="text" id="penerbit" name="penerbit" class="form-control"></td>
						</tr>
						<tr>
							<td>Harga</td>
							<td><input type="number" id="harga" name="harga" class="form-control"></td>
						</tr>
						<tr>
							<td>Diskon</td>
							<td><input type="number" id="diskon" name="diskon" class="form-control"></td>
						</tr>
						<tr>
							<td>Stok</td>
							<td><input type="number" id="stok" name="stok" class="form-control"></td>
						</tr>
					</table>
					<input type="submit" name="simpan" value="Simpan" class="btn btn-success">
				</form>
			</div>
		</div>		
	</div>
</div>
<script>
	function edit(a) {
		$.ajax({
			type:"post",
			url:"<?=base_url()?>index.php/buku/edit_buku/"+a,
			dataType:"json",
			success:function(data){
				$("#kode_buku").val(data.kode_buku);
				$("#judul_buku").val(data.judul_buku);
				$("#kode_kategori").val(data.kode_kategori);
				$("#tahun").val(data.tahun);
				$("#penulis").val(data.penulis);
				$("#penerbit").val(data.penerbit);
				$("#harga").val(data.harga);
				$("#diskon").val(data.diskon);
				$("#stok").val(data.stok);
			}
		});
	}
</script>