<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function index()
	{
		$this->load->view('v_login');
		
	}
	public function proses_login()
	{
		if ($this->input->post('login')) {
			$this->form_validation->set_rules('username', 'username', 'trim|required');
			$this->form_validation->set_rules('password', 'password', 'trim|required');
			if ($this->form_validation->run() == TRUE) {
				$this->load->model('m_login');
				if ($this->m_login->get_login()->num_rows()>0) {
					$data=$this->m_login->get_login()->row();
					$array = array(
						'login' => TRUE,
						'username' => $data->username ,
						'password' => $data->password,
						'level' => $data->level,
						'kode_user' => $data->kode_user,
						'nama_user' => $data->nama_user
					);
					
					$this->session->set_userdata( $array );
					redirect('kategori','refresh');
				}else{
					$this->session->set_flashdata('pesan', 'salah username dan password');
					redirect('login','refresh');
				}
			} else {
				$this->session->set_flashdata('pesan', validation_errors());
				redirect('login','refresh');
			}
		}
	}
	public function logout()
	{
		session_destroy();
		redirect('login','refresh');
	}

}

/* End of file login.php */
/* Location: ./application/controllers/login.php */