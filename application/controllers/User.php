<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct()
{
	parent::__construct();
	$this->load->model('m_user','kas');
}
	public function index()
	{
		$data['tampil_user']=$this->kas->tampil_kas();
		$data['konten']="v_user";
		$this->load->view('template', $data);
	}
	public function tambah()
	{
		if ($this->input->post('simpan')) {
			if ($this->kas->simpan_kas()) {
				$this->session->set_flashdata('pesan', 'menambah user');
				redirect('user','refresh');
			}else{
				$this->session->set_flashdata('pesan', 'Gagal menambah user');
				redirect('kuser','refresh');
			}
		}
	}
	public function edit_user($id)
	{
		$data=$this->kas->detail($id);
		echo json_encode($data);
	}
	public function user_update()
	{
		if ($this->input->post('edit')) {
			if ($this->kas->edit_kas()) {
				$this->session->set_flashdata('pesan', 'Sukses Hapus ');
				redirect('user','refresh');
			}else{
				$this->session->set_flashdata('pesan', 'Gagal Hapus ');
				redirect('user','refresh');
			}
		}
	}
	public function hapus($id='')
	{
		if ($this->kas->hapus_kas($id)) {
			$this->session->set_flashdata('pesan', 'Sukses Hapus ');
			redirect('user','refresh');
		}else{
			$this->session->set_flashdata('pesan', 'Gagal Hapus ');
			redirect('user','refresh');
		}
	}

}

/* End of file User.php */
/* Location: ./application/controllers/User.php */